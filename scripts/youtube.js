
if (typeof browser === 'undefined') {
	browser = chrome
}

/* - Use theater mode - C1 */
function activateTheaterMode(){
  if(document.getElementsByClassName('ytp-size-button')[0] != undefined){
    let theaterModeButton = document.getElementsByClassName('ytp-size-button')[0];
    if(theaterModeButton.getElementsByTagName("path")[0].getAttribute("d") === "m 28,11 0,14 -20,0 0,-14 z m -18,2 16,0 0,10 -16,0 0,-10 z"){
      theaterModeButton.click();
    }
  }
}

/* - Remove autoplay "feature" - U1 */
function deactivateAutoplay(){
	let autoplayButton = document.getElementsByClassName('ytp-autonav-toggle-button')[0];
	if(autoplayButton){
		if(autoplayButton.getAttribute("aria-checked") == "true"){
			autoplayButton.click();
			return true;
		}
	}
	return false;
}

if(!deactivateAutoplay()){

	const autoplayButtonContainer = document.getElementsByClassName("ytp-autonav-toggle-button-container")[0];

	const deactivateAutoplayObserver = new MutationObserver((mutationList, observer) => {
		for (const mutation of mutationList) {
			if(deactivateAutoplay()){
				deactivateAutoplayObserver.disconnect();
			}
		}
	});
	if(autoplayButtonContainer){
		deactivateAutoplayObserver.observe(autoplayButtonContainer, { attributes: true, childList: true, subtree: true, attributeFilter:["aria-checked"] });
	}
}

/* - Replace the subscription list from the side menu by a link to the subscription manager - U3 */
function replaceSubscriptionManager(){
	if(document.getElementById("avatar-btn") || document.getElementById("yt-masthead-account-picker")){
		let subGuide =
			document.getElementsByTagName("ytd-guide-section-renderer").length ?
				document.getElementsByTagName("ytd-guide-section-renderer")[1] :
				document.getElementById("guide-subscriptions-section");

		let subManagerLink = document.createElement('a');
		subManagerLink.setAttribute('href', 'https://www.youtube.com/subscription_manager');
		subGuide.getElementsByTagName("div")[0].style.display="none";

		let subGuideTitle = subGuide.getElementsByTagName("h3")[0];

		if(subGuideTitle.getElementsByTagName("yt-formatted-string").length > 0){
			subGuideTitle.getElementsByTagName("yt-formatted-string")[0].style.textTransform="capitalize";
		} else{
			subGuideTitle.style.textTransform = "capitalize";
			subGuideTitle.style.color = "dimgrey";
		}

		subManagerLink.appendChild(subGuideTitle);

		if(subGuide.getElementsByTagName("hr").length){
			subGuide.insertBefore(subManagerLink, subGuide.getElementsByTagName("hr")[0]);
		} else {
			subGuide.appendChild(subManagerLink);
		}
	}
}

/* - Hide the live chat from the user and replace it with its button - U2 */
function hideLiveChat(){
  if(document.getElementById("chat") !== null){
    let iteration = 0;
    let clicked = false;

    if(document.getElementById("chatframe")){
      var checkInterval = setInterval(function(){
        let chatFrame = document.getElementById("chatframe");
        iteration++;

        // Prevent the interval from running too much time
        if(iteration > 20){
          console.error("[minimal] YT live chat error: Loading time exceeded 10s");
          clearInterval(checkInterval);
          return false;
        }

        // Check the load status of the live frame
        if(chatFrame.contentDocument.readyState == "complete"){
          if(document.getElementById("show-hide-button")){
            let chatButton = document.getElementById("show-hide-button").getElementsByTagName("button")[0];

            // Do a click once on the chat button if the chat is not hidden
            if(chatButton.hasAttribute("aria-pressed")){
              if(chatButton.getAttribute("aria-pressed") == "false"){
                chatButton.click();
              }
              else{
                clicked = true;
                clearInterval(checkInterval);
              }
            }
            else{
              if(chatFrame.contentDocument.body.childElementCount > 0 && !clicked){
                chatButton.click();
                clearInterval(checkInterval);
              }
            }
          }
        }
      },500);
    }
  }
}

/* - Add toggle for homepage recommended videos - U2 */
function addRecommendedVideoToggle(){
	console.log("[minimal] Add toggle for homepage recommended videos");
	let home_page = document.querySelector('[page-subtype="home"]');
	let home_page_children = document.querySelectorAll('[page-subtype="home"]>*');
	home_page_children.forEach(function(child){
		child.style.visibility = "hidden";
		child.style.height = "0";
	});
	let show_videos_button = document.createElement("button");

	show_videos_button.appendChild(document.createTextNode(browser.i18n.getMessage("toggleOn") + " " + browser.i18n.getMessage("videoRecommendations")));
	show_videos_button.style.position="fixed";
	show_videos_button.style.bottom="1em";
	show_videos_button.style.right="1em";
	show_videos_button.addEventListener("click", function(event){
		if(event.target.textContent == browser.i18n.getMessage("toggleOn") + " " + browser.i18n.getMessage("videoRecommendations")){
				home_page_children.forEach(function(child){
					child.style.visibility = "";
					child.style.height = "";
				});
			event.target.textContent = browser.i18n.getMessage("toggleOff") + " " + browser.i18n.getMessage("videoRecommendations");
		} else {
				home_page_children.forEach(function(child){
					child.style.visibility = "hidden";
					child.style.height = "0";
				});
			event.target.textContent = browser.i18n.getMessage("toggleOn") + " " + browser.i18n.getMessage("videoRecommendations");
		}
	})
	home_page.appendChild(show_videos_button);
}

function addRecommendedVideoToggleTry(){
	let home_page = document.querySelector('[page-subtype="home"]');
	if(home_page){
		addRecommendedVideoToggle();
		return true;
	}
	setTimeout(addRecommendedVideoToggleTry, 500);
}

addRecommendedVideoToggleTry();

// Execute all the functions here
function execute(){
  // Use theater mode
  activateTheaterMode();
  
  // Hide live chat once after the video loaded
  setTimeout(hideLiveChat, 500);
  
  // Replace the subscription list
  replaceSubscriptionManager();
}

var observer = new MutationObserver(
	function(mutationList){
		observed = mutationList[0].target.attributes["aria-valuenow"];
		if(observed.nodeValue == "100"){execute()}
	});

var domInterval = setInterval(function(){
  var targetNode = document.querySelector("yt-page-navigation-progress");
  if(targetNode){
    observer.observe(targetNode, {childList:true, attributes:true, characterData:true, subtree:false, attributeFilter:["aria-valuenow"]});
		clearInterval(domInterval);
  }
}, 500);

setTimeout(execute, 500);

window.addEventListener("load", execute);
